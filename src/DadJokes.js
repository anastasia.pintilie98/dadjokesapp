import React, { Component } from "react";
import axios from "axios";
import { v4 as uuid } from "uuid";
import Joke from "./Joke.js";
import "./DadJokes.css";
import { withStyles } from "@material-ui/styles";
import sizes from "./Styles/sizes";
import Button from "@material-ui/core/Button";
import FlipMove from 'react-flip-move';

const styles = {
  
  
    '@keyframes spin': {
      from: { transform: "rotate(0deg)"},
      to: { transform: "rotate(360deg)"}
    },
  
  DadJokes: {
    display: "flex",
    width: "80vw",
    height: "70vh",
    justifyContent: "center",

    [sizes.down("md")]: {
      flexDirection: "column",
      height: "100vh",
      width: "100vw",
    },
  },

  DadJokesSidebar: {
    background: "#9575cd",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    width: "30%",
    boxShadow: "-8px 8px 11px 3px black",
    color: "white",
    "& img": {
      maxWidth: "210px",
      borderRadius: "50%",
      boxShadow: "-8px 8px 11px 3px rgba(0, 0, 0, 0.43)",

      [sizes.down("lg")]: {
        maxWidth: "70px",
      },
    },
    [sizes.down("md")]: {
      "& h1":{
        fontSize:"20px"
      },
      width: "100vw",
      height: "25vh",
      alignSelf: "flex-start",
    },
  },

  DadJokesJokes: {
    background: "white",
    display: "flex",
    flexDirection: "column",
    overflow: "scroll",
    alignSelf: "center",
    width: "100%",
    height: "100%",
    [sizes.down("md")]: {
      alignSelf: "flex-end",
      height: "100%",
      width: "100%",
    },
  },

  emSweatSmile: {
    alignSelf: "center",
    fontSize: "100px",
    animationName: "spin",
    animationDuration: "5000ms",
    animationIterationCount: "infinite",
    animationTimingFunction: "linear",
    margin: "0 auto",
  },
  LoadingZone: {
    display: "flex",
    flexDirection: "column",
    alignContent: "center",
    justifyContent: "center",
    fontFamily: "Rubik, sans-serif",
    color: "white",
    textAlign: "center",
    "& h2": {
      alignSelf: "center",
      color: "white",
      fontWeight: "600",
      fontSize: "40px",
      letterSpacing: "2px",
    },
    "& i": {
      alignSelf: "center",
      fontSize: "100px",
      animationName: "$spin",
      animationDuration: "5000ms",
      animationIterationCount: "infinite",
      animationTimingFunction: "linear",
      margin: "0 auto",
    },
  },
  getMore: {
    margin: "1em 0 1em 0",
  },
};

class DadJokes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loadedJokes: JSON.parse(window.localStorage.getItem("jokes") || "[]"),
      isLoading: false,
    };
    this.seenJokes = new Set(this.state.loadedJokes.map((joke) => joke.text));
    this.handleClick = this.handleClick.bind(this);
  }

  componentDidMount() {
    if (this.state.loadedJokes.length === 0) {
      this.getJokes();
    }
  }

  async getJokes() {
    try {
      let jokes = [];
      this.setState({ isLoading: true });
      while (jokes.length < 10) {
        var newJoke = await axios.get("https://icanhazdadjoke.com/", {
          headers: { Accept: "application/json" },
        });
        console.log(newJoke.data.joke);
        let newObj = { text: newJoke.data.joke, score: 0, id: uuid() };
        if (!this.seenJokes.has(newJoke.data.joke)) jokes.push(newObj);
        else console.log("Found duplicate");
        if (jokes.length === 10) this.setState({ isLoading: false });
      }
      this.setState(
        (prevState) => ({
          loadedJokes: [...prevState.loadedJokes, ...jokes],
          isLoading: false,
        }),
        () =>
          window.localStorage.setItem(
            "jokes",
            JSON.stringify(this.state.loadedJokes)
          )
      );
    } catch (e) {
      alert(e);
    }
  }

  handleVote(idx, value) {
    this.setState(
      (prev) => ({
        loadedJokes: prev.loadedJokes.map((j) =>
          j.id === idx ? { ...j, score: j.score + value } : j
        ),
      }),
      () =>
        window.localStorage.setItem(
          "jokes",
          JSON.stringify(this.state.loadedJokes)
        )
    );
  }
  handleClick() {
    this.getJokes();
  }

  render() {
    let { classes } = this.props;
    let glume = this.state.loadedJokes.sort((a, b) => {
      return b.score - a.score;
    });
    console.log("in render");
    console.log(glume);
    if (this.state.isLoading)
      return (
        <div className={classes.LoadingZone}>
          <i
            className="em em-sweat_smile"
            aria-role="presentation"
            aria-label="SMILING FACE WITH OPEN MOUTH AND COLD SWEAT"
          ></i>
          <h2>Loading...</h2>
        </div>
      );
    return (
      <div className={classes.DadJokes}>
        <div className={classes.DadJokesSidebar}>
          <h1>
            <span>Dad</span> Jokes
          </h1>
          <img src="https://assets.dryicons.com/uploads/icon/svg/8927/0eb14c71-38f2-433a-bfc8-23d9c99b3647.svg" />
          <Button
            className={classes.getMore}
            variant="contained"
            color="secondary"
            onClick={this.handleClick}
          >
            New jokes
          </Button>
        </div>
        <div className={classes.DadJokesJokes}>
          <FlipMove>
          {glume.map((joke) => (
            <Joke
              text={joke.text}
              score={joke.score}
              key={joke.id}
              upvote={() => this.handleVote(joke.id, 1)}
              downvote={() => this.handleVote(joke.id, -1)}
            />
          ))}
          </FlipMove>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(DadJokes);
