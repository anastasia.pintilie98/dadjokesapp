import React,{Component} from 'react';
import './Joke.css'
class Joke extends Component {
    render()
    {
        let emoji='';
        let scoreColor='';
        if(this.props.score<0)
        {
            emoji='angry';
            scoreColor='red';
        }
        else if(this.props.score>=0 && this.props.score<5)
        {
            emoji='disappointed';
            scoreColor='orange'
        }
        else if(this.props.score>=5 && this.props.score<8)
        {
            emoji='expressionless';
            scoreColor='yellow'
        }
        else if(this.props.score>=8 && this.props.score<=12)
        {
            emoji='smile';
            scoreColor='light-green'
        }
        else if(this.props.score>12)
        {
            emoji='laughing';
            scoreColor='green'
        }
        return <div className='Joke'>
            <i className="fas fa-arrow-down" onClick={this.props.downvote}>
                </i><p className='scor' style={{border:`2px solid ${scoreColor}`}}>{this.props.score}</p> 
            <i className="fas fa-arrow-up" onClick={this.props.upvote}></i>
            <div className='Joke-text'>
            <p>{this.props.text}</p>
            </div>
            <div className='Joke-smiley'>
            <i className={`em em-${emoji}`} aria-role="presentation" aria-label="ANGRY FACE"></i>
            </div>
            
        </div>
    }
}

export default Joke;