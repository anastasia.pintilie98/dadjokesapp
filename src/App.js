import './App.css';
import DadJokes from './DadJokes';
import React, {Component} from 'react';
import {mergeClasses, withStyles} from '@material-ui/styles'
import sizes from './Styles/sizes';
import { Hidden } from '@material-ui/core';

const styles ={
 App:{
  display: "flex",
  alignItems: "center",
  height:"100vh",
  justifyContent: "center",
  background: "linear-gradient(125deg,  #ABE1FC 0%,  #ABE1FC 50%,  #F05688 50%, #F05688 100%)",
  backgroundColor:"red",
  fontFamily: 'Rubik, sans-serif',
  [sizes.down("md")]:{
    height:"100vh",
    width:"100vw",
  }
 }
}
class App extends Component {
 render()
 {
  let  {classes}=this.props;
  return (
    <div className={classes.App}>
   <DadJokes/>
    </div>
  );
 }
}

export default withStyles(styles)(App);
